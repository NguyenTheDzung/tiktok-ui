import classNames from "classnames/bind";
import styles from './AccountItem.module.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons";
import Avatar1 from '~/assets/images/1.png';

const cx = classNames.bind(styles)
function AccountItem() {
    return(
        <div className={cx('wrapper')}>
            <img className={cx('avatar')} src={Avatar1} alt="" />
            <div className={cx('info')}>
                <h4 className={cx('name')}>
                    <span>!!!</span>
                    <FontAwesomeIcon icon={faCheckCircle} />
                </h4>
                <span className={cx('username')}>...</span>
            </div>
        </div>
    )
}

export default AccountItem;